import unittest
from test.userlogin import student_parame,get_students_token
import requests,random,string
from  config import  *
def randomtext(lengths=4):
    return ''.join(random.sample(string.ascii_letters + string.digits, lengths))


class TestMessageCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.url = testplan+'/user/addmessage'
        cls.token = get_students_token(student_parame)
    @classmethod
    def tearDownClass(cls) -> None:
        cls.url=''
    def setUp(self) -> None:
        pass
    def tearDown(self) -> None:
        self.text=''
    def testuser_not_login(self):
        reponse=requests.post(self.url)
        status=reponse.status_code
        reslut=reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 421)
    def test_user_notexc(self):
        headers = {
            "token": self.token
        }
        data = {
            'id': 0,
            'connect': randomtext(6)
        }

        reonse=requests.post(self.url,json=data,headers=headers)
        status=reonse.status_code
        reslut=reonse.json()
        print(reslut)
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100503)
        self.assertEqual(reslut['message'], '留言用户不存在')

    def test_user_file(self):
        headers = {
            "token": self.token
        }
        data={
            'id':userid,
            'connect':randomtext(4)
        }
        reonse=requests.post(self.url,json=data,headers=headers)
        status=reonse.status_code
        reslut=reonse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100502)
        self.assertEqual(reslut['message'], '留言长度在5-500个字符长度')
    def test_user_twod_file(self):
        headers = {
            "token": self.token
        }
        data={
            'id':userid,
            'connect':randomtext(10)*100
        }
        reonse=requests.post(self.url,json=data,headers=headers)
        status=reonse.status_code
        reslut=reonse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100502)
        self.assertEqual(reslut['message'], '留言长度在5-500个字符长度')
    def test_message_two_me(self):
        headers = {
            "token": self.token
        }
        data={
            'id':user_cru,
            'connect':randomtext(10)
        }
        reonse=requests.post(self.url,json=data,headers=headers)
        status=reonse.status_code
        reslut=reonse.json()
        print(reslut)
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100501)
        self.assertEqual(reslut['message'], '自己不能给自己留言')
    def test_message_to_orther(self):
        headers = {
            "token": self.token
        }
        data={
            'id':4,
            'connect':randomtext(10)
        }
        reonse=requests.post(self.url,json=data,headers=headers)
        status=reonse.status_code
        reslut=reonse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 200)
        self.assertEqual(reslut['message'], '成功')

if __name__ == '__main__':
    unittest.main()
