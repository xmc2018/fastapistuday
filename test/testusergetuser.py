'''
  @Description      
  @auther         leizi
'''
import  unittest,requests
from test.userlogin import *
from config import testplan
from test.userlogin import student_parame,teacher_parame
class UserGetCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:

        cls.url = testplan+'/user/getcuruser/'

    @classmethod
    def tearDownClass(cls) -> None:
        '''
        还原测试环境，测试url
        :return:
        '''
        cls.url = ''
    def test_usernotLogin(self)->None:
        reponse = requests.get(self.url)
        status = reponse.status_code
        reslut = reponse.json()
        self.assertEqual(status,200)
        self.assertEqual(reslut['code'],421)
    def test_user_not_exict(self)->None:

        self.token = get_students_token(student_parame)
        headers={
            "token":self.token
        }
        reponse = requests.get(self.url,headers=headers)
        reslut = reponse.json()
        self.assertEqual(reponse.status_code,200)
        self.assertEqual(reslut['message'],'成功')
        self.assertEqual(reslut['data']['username'],student_parame['username'])
    def  test_get_teacher(self):
        self.token = get_teacher_tone(teacher_parame)
        headers = {
            "token": self.token
        }
        reponse = requests.get(self.url, headers=headers)
        reslut = reponse.json()
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(reslut['message'], '成功')
        self.assertEqual(reslut['data']['username'], teacher_parame['username'])
        self.assertIn("jobnum",reslut['data'])
if __name__ == "__main__":
    unittest.main()