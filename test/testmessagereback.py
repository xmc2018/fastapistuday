import unittest
from test.userlogin import student_parame,get_students_token
import requests,random,string
from  test.pyopearmysql import *
def randomtext(lengths=4):
    return ''.join(random.sample(string.ascii_letters + string.digits, lengths))

class TestMessageListCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.url = testplan+'/user/rebackmessage'
        cls.token = get_students_token(student_parame)
        cls.connct = OpearMysql()
    @classmethod
    def tearDownClass(cls) -> None:
        cls.url=''
    def setUp(self) -> None:
        pass
    def tearDown(self) -> None:
        self.text=''
    def testuser_not_login(self):
        reponse=requests.post(self.url)
        status=reponse.status_code
        reslut=reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 421)

    def testuser_login_less(self):
        headers = {
            "token": self.token
        }
        data={
            'rebackid':1,
            'connect':randomtext(4),
            "id":1
        }
        reponse = requests.post(self.url,json=data,headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        print(reslut)
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100803)
        self.assertEqual(reslut['message'], "回复内容应该在5-500字")
    def testuser_login_big(self):
        headers = {
            "token": self.token
        }
        data={
            'rebackid':1,
            'connect':randomtext(4)*200,
            "id":1
        }
        reponse = requests.post(self.url,json=data,headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        print(reslut)
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100803)
        self.assertEqual(reslut['message'], "回复内容应该在5-500字")
    def testuser_success(self):
        headers = {
            "token": self.token
        }
        data={
            'rebackid':1,
            'connect':randomtext(8),
            "id":1
        }
        reponse = requests.post(self.url,json=data,headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 200)
        self.assertEqual(reslut['message'], "成功")
    def testuser__not(self):
        headers = {
            "token": self.token
        }
        selectid = self.connct.run(
            "SELECT messages.id FROM messages WHERE senduser in (SELECT id FROM users  WHERE username !='liwanle1i' )")
        data={
            'rebackid':int(selectid[-1][0]) + 1,
            'connect':randomtext(8),
            "id":int(selectid[-1][0]) + 1
        }
        reponse = requests.post(self.url,json=data,headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100804)
        self.assertEqual(reslut['message'], "回复留言id不存在")
if __name__ == '__main__':
    unittest.main()
