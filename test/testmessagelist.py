import unittest
from test.userlogin import student_parame,get_students_token
import requests
from  config import  *

class TestMessageListCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.url = testplan+'/user/messagelist'
        cls.token = get_students_token(student_parame)
    @classmethod
    def tearDownClass(cls) -> None:
        cls.url=''
    def setUp(self) -> None:
        pass
    def tearDown(self) -> None:
        self.text=''
    def testuser_not_login(self):
        reponse=requests.get(self.url)
        status=reponse.status_code
        reslut=reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 421)

    def testuser_login_id(self):
        headers = {
            "token": self.token
        }
        reponse = requests.get(self.url,headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 200)

if __name__ == '__main__':
    unittest.main()
