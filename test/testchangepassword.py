import unittest
from copy import deepcopy
from test.userlogin import *
from config import  testplan
class TestpasswordCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.url = testplan+'/user/changepassword'
        cls.student_parame = {
            "username": "liwanle1i",
            "password": "012345678910",
            "newpassword": "012345678910"
        }
    @classmethod
    def tearDownClass(cls) -> None:
        cls.url=""
    def setUp(self) -> None:
        pass
    def tearDown(self) -> None:
        toekn = get_students_token(self.student_parame)
        headers = {
            "token": toekn
        }
        self.student_parame["newpassword"] = '012345678910'
        requests.post(self.url, json=self.student_parame, headers=headers)
        self.student_parame["password"] = '012345678910'


    def test_userNOtlogin(self):
        reponse = requests.post(self.url)
        status = reponse.status_code
        reslut = reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 421)
    def test_student_error(self):
        self.student_parame['newpassword']='1234567'
        toekn=get_students_token(self.student_parame)
        headers = {
            "token": toekn
        }
        reponse = requests.post(self.url,json=self.student_parame,headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100303)
    def test_student_user(self):
        toekn = get_students_token(self.student_parame)
        headers = {
            "token": toekn
        }
        reponse = requests.post(self.url,json=self.student_parame,headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100304)
        self.assertEqual(reslut['message'], "新旧密码不能一样")
    def test_student_new(self):
        self.student_parame['newpassword'] = '1234567'
        toekn = get_students_token(self.student_parame)
        headers = {
            "token": toekn
        }
        reponse = requests.post(self.url, json=self.student_parame, headers=headers)
        status = reponse.status_code
        reslut = reponse.json()

        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100303)
        self.assertEqual(reslut['message'], "新密码长度不匹配")
    def test_student_newS(self):
        self.student_parame['newpassword'] = '0123456789101'
        toekn = get_students_token(self.student_parame)
        headers = {
            "token": toekn
        }
        reponse = requests.post(self.url, json=self.student_parame, headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        self.student_parame['password'] = '0123456789101'
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 200)
        self.assertEqual(reslut['message'], "成功")

    def test_student_not(self):
        toekn = get_students_token(self.student_parame)
        headers = {
            "token": toekn
        }
        self.name=deepcopy(self.student_parame)
        self.name['password']='1212132213'
        self.name['newpassword']='111111111111'
        reponse = requests.post(self.url, json=self.name, headers=headers)
        status = reponse.status_code
        reslut = reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100301)
        self.assertEqual(reslut['message'], "原密码校验失败")



if __name__ == '__main__':
    unittest.main()
