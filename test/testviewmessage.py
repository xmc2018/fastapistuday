import unittest
from test.userlogin import student_parame,get_students_token
import requests
from test.pyopearmysql import  OpearMysql
from config import testplan
class TestViesMessageCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.url = testplan+'/user/viewmessage'
        cls.token = get_students_token(student_parame)
        cls.connct=OpearMysql()
    @classmethod
    def tearDownClass(cls) -> None:
        cls.url=''
    def setUp(self) -> None:
        pass
    def tearDown(self) -> None:
        self.text=''
    def testuser_not_login(self):
        reponse=requests.get(self.url)
        status=reponse.status_code
        reslut=reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 421)
    def testuser_login_id_not(self):
        headers = {
            "token": self.token
        }
        selectid = self.connct.run(
            "SELECT messages.id FROM messages WHERE senduser in (SELECT id FROM users  WHERE username !='liwanle1i' )")
        data = {'id': int(selectid[-1][0])+1}
        reponse=requests.get(self.url,params=data,headers=headers)
        status=reponse.status_code
        reslut=reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100601)
        self.assertEqual(reslut['message'], '留言不存在')
    def testuser_login_id_per(self):
        headers = {
            "token": self.token
        }
        selectid=self.connct.run("SELECT messages.id FROM messages WHERE senduser in (SELECT id FROM users  WHERE username !='liwanle1i' )")
        data={'id':selectid[0][0]}
        reponse=requests.get(self.url,params=data,headers=headers)
        status=reponse.status_code
        reslut=reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 100602)
        self.assertEqual(reslut['message'], '权限不足')
    def testuser_login_id_success(self):
        headers = {
            "token": self.token
        }
        selectid = self.connct.run(
            "SELECT messages.id FROM messages WHERE senduser in (SELECT id FROM users  WHERE username ='liwanle1i' and status=0 )")
        id=(selectid[0][0])
        data={'id':id}
        reponse=requests.get(self.url,params=data,headers=headers)
        status=reponse.status_code
        reslut=reponse.json()
        self.assertEqual(status, 200)
        self.assertEqual(reslut['code'], 200)
        self.assertEqual(reslut['message'], '成功')

if __name__ == '__main__':
    unittest.main()
