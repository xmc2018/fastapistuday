'''
  @Description      
  @auther         leizi
'''
from pydantic import BaseModel,Field
from typing import Optional, List


class UserBase(BaseModel):
    username: str


class UserCreate(UserBase):
    password: str
    role: str
    jobnum: Optional[int] = None
    studentnum: Optional[int] = None
    sex: str = "男"
    age: int


class UserLogin(UserBase):
    password: str


class UsersToken(UserBase):
    token: str


class UsernameRole(UserBase):
    role: str


class UserChangepassword(BaseModel):
    password: str
    newpassword: str


class MessageConent(BaseModel):
    id: int
    connect: str


class RebackMessConnet(MessageConent):
    rebackid: int


class Messages(BaseModel):
    id: int
    senduser: str
    acceptusers: str
    read: bool
    sendtime: str
    addtime: str
    context: str


class MessagePid(Messages):
    pid: int


class MessageOne(Messages):
    pid: List[MessagePid] = []


class Courses(BaseModel):
    name: str
    icon: Optional[str]
    desc: Optional[str]
    catalog: Optional[str]
    onsale: Optional[int]
    owner: str
    likenum: int


class CoursesCommentBase(BaseModel):
    users: str
    pid: int
    addtime: str
    context: str


class Coursescomment(CoursesCommentBase):
    id: int
    top: int


class CoursesEdit(Courses):
    id:int

class CousesDetail(Courses):
    id:int
    commonet: List[Coursescomment] = []


class Coursecomment(BaseModel):
    id: int
    comments: str
    pid: Optional[int]

